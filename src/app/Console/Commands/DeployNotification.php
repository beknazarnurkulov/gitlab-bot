<?php

namespace App\Console\Commands;

use App\Models\Chat;
use Illuminate\Console\Command;
use Telegram\Bot\Api;

class DeployNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:deployed {projectName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deploy notification';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Api $telegram)
    {
        $projectName = $this->argument('projectName');

        $chats = Chat::whereJsonContains('projects', [$projectName])->get();
        if (!$chats) {
            return;
        }

        $message = "Проект $projectName успешно задеплоен в автоматическом режиме.";

        foreach ($chats as $chat) {
            $chatId = $chat->chat_id;
            $telegram->sendMessage([
                'chat_id' => $chatId,
                'text' => $message
            ]);
        }
    }
}
