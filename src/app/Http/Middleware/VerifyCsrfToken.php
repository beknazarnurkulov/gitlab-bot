<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    protected $except = [
        "/6193603140:AAHmOLgZsFTz5Tf2r0IRzJn9n3nlXwh0a38/webhook",
        '/gitlab-notification'
    ];
}
