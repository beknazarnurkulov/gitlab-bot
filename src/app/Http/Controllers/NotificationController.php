<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;

class NotificationController extends Controller
{
    public function notification(Request $request, Api $telegram)
    {
        $request = $request->all();
        $projectName = $request['project']['name'];
        $userName = $request['user_name'];
        $commits = $request['commits'];

        $chats = Chat::whereJsonContains('projects', [$projectName])->get();
        if (!$chats) {
            return;
        }

        foreach ($chats as $chat) {
            $message = "Проект: $projectName\nПользователь: $userName\nСделаны коммиты для основной ветки:";

            $chatId = $chat->chat_id;
            foreach ($commits as $commit) {
                $message .= "\n - " . $commit['message'];
            }

            $telegram->sendMessage([
                'chat_id' => $chatId,
                'text' => $message
            ]);
        }
    }
}
