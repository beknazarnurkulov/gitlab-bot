<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Chat;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Laravel\Facades\Telegram;

class ChatController extends Controller
{
    public function chat()
    {
        $updates = Telegram::getWebhookUpdate();

        if ($updates->message->chat->type != 'group') {
            return;
        }

        if (Chat::where('chat_id', $updates->message->chat->id)->exists()) {
            $chat = Chat::where('chat_id', $updates->message->chat->id)->first();
            $chat->group = $updates->message->chat->title;
            $chat->save();
        } else {
            Chat::create([
                'group' => $updates->message->chat->title,
                'chat_id' => $updates->message->chat->id,
            ]);
        }

        if (!$updates->message || !$updates->message->text) {
            return;
        }

        $message = $updates->message->text;
        if (strpos($message, '/add#') === 0) {
            $projectName = explode('#', $message)[1];
            $projects = $chat->projects;
            if (!$projects) {
                $projects = [];
            }
            if (!in_array($projectName, $projects)) {
                $projects[] = $projectName;
            }
            $chat->projects = $projects;
            $chat->save();
            Telegram::sendMessage([
                'chat_id' => $updates->message->chat->id,
                'text' => 'Вы успешно добавили проект ' . $projectName . ' для уведомлений в этой группе.'
            ]);
        } else if (strpos($message, '/remove#') === 0) {
            $projectName = explode('#', $message)[1];
            $projects = $chat->projects;
            if (!$projects) {
                $projects = [];
            }
            $projects = array_filter($projects, function ($project) use ($projectName) {
                return $project != $projectName;
            });
            $chat->projects = $projects;
            $chat->save();
            Telegram::sendMessage([
                'chat_id' => $updates->message->chat->id,
                'text' => 'Вы успешно удалили проект ' . $projectName . ' от уведомлений.'
            ]);
        } else if (strpos($message, '/list') === 0) {
            $projects = $chat->projects;
            if (!$projects) {
                $projects = [];
            }
            $text = 'Список проектов для уведомлений в этой группе:';
            foreach ($projects as $project) {
                $text .= "\n" . $project;
            }
            Telegram::sendMessage([
                'chat_id' => $updates->message->chat->id,
                'text' => $text
            ]);
        } else if (strpos($message, '/help') === 0) {
            $text = 'Список команд:
                /add#projectName - добавить проект для уведомлений в этой группе (например, /add#projectName)
                /remove#projectName - удалить проект от уведомлений в этой группе (например, /remove#projectName)
                /list - список проектов для уведомлений в этой группе
                /help - список команд';
            Telegram::sendMessage([
                'chat_id' => $updates->message->chat->id,
                'text' => $text
            ]);
        } else {
            Telegram::sendMessage([
                'chat_id' => $updates->message->chat->id,
                'text' => 'Неизвестная команда. Для получения списка команд введите /help'
            ]);
        }

    }
}
