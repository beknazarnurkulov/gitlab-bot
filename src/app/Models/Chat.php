<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $fillable = [
        'group',
        'chat_id',
        'projects',
    ];

    protected $casts = [
        'projects' => 'array'
    ];
}
