<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\NotificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/6193603140:AAHmOLgZsFTz5Tf2r0IRzJn9n3nlXwh0a38/webhook', [
    ChatController::class, 'chat'
]);

Route::post('/gitlab-notification', [
    NotificationController::class, 'notification'
]);
